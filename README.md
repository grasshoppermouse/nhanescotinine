# README #

This data package combines the continuous NHANES data
from 1999-2010 for cotinine, basic demographics, and reproductive variables,
and it computes the 12 year weights needed to make correct inferences to the US population.